# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.7.1
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

# # Semi-analytical benchmarks

# ## 0. Import some Python packages

# +
import time
import numpy as np
import matplotlib.pyplot as plt
import analytic_2D_function as a2D
from tqdm import trange

plt.rcParams["font.family"] = "Times"
plt.rcParams.update({"font.size": 50})
plt.rcParams["xtick.major.pad"] = "12"
plt.rcParams["ytick.major.pad"] = "12"
# -

# ## 1. Input parameters

# +
# Looped input. ============================================

# Number of sources to be tested.
nsrc = [
    200,
    250,
    300,
    350,
    400,
    450,
    500,
    600,
    700,
    800,
    1000,
    2000,
    4000,
]  # disable long runs for CI testing
# Number of realisations to be tested.
N = [10, 20, 50, 100, 300, 500]  # disable long runs for CI testing
# Number of repeats.
Nr = 5

# Fixed input. =============================================

# Number of transit times in the time series.
L = 100.0
# Frequency band [Hz].
freqmin = 100.0e3
freqmax = 500.0e3
# -

### for CI testing
a = 1
b = 1
### to reproduce figures
# a = len(nsrc)
# b = len(N)

# ## 2. Error analysis

# In the following, we compute the traveltime errors as a function of number  of  sources and number of wavefield realisations for an ensemble of independent runs.

# + tags=[]
# Loop over everything.

ddtt_rel = np.zeros([len(N), len(nsrc), Nr])

for r in range(Nr):
    for i in range(len(nsrc[:a])):
        for j in range(len(N[:b])):

            t1 = time.time()
            print("r=%d, nsrc=%d, N=%d" % (r, nsrc[i], N[j]))
            ddtt, ddtt_rel[j, i, r], dv = a2D.tt_error(
                nsrc=nsrc[i], N=N[j], L=L, freqmin=freqmin, freqmax=freqmax
            )
            print("absolute time shift error: %g ms" % ddtt)
            print("relative time shift error: %g percent" % ddtt_rel[j, i, r])
            print("velocity error: %g m/s" % dv)
            t2 = time.time()
            print("elapsed time: %f s" % (t2 - t1))
            print("-----------------------------------------------------------")
# -

# ## 3. Compute average traveltime difference error

# +
ddtt_rel_avg = np.zeros([len(N), len(nsrc)])

for i in range(len(nsrc)):
    for j in range(len(N)):
        ddtt_rel_avg[j, i] = np.sum(np.abs(ddtt_rel[j, i, :])) / float(Nr)


# Plot relative traveltime errors.

fig, ax1 = plt.subplots(1, figsize=(25, 15))

for i in range(len(N)):

    c = 0.85 - 0.85 * float(i) / float(len(N))

    ax1.plot(
        nsrc,
        np.max(np.abs(ddtt_rel[i, :, :]), axis=1),
        "--",
        color=[c, c, c],
        linewidth=4,
    )
    ax1.plot(nsrc, np.abs(ddtt_rel_avg[i, :]), color=[c, c, c], linewidth=10)
    ax1.text(1.01 * nsrc[-1], np.abs(ddtt_rel_avg[i, -1]), str(N[i]))

ax1.set_xlabel("number of sources", labelpad=15)
ax1.set_ylabel("rel. time error [%]", labelpad=15)

# Make secondary axis for velocity error.


def tt2v(x):
    return 15.0 * x


def v2tt(x):
    return x / 15.0


secax = ax1.secondary_yaxis("right", functions=(tt2v, v2tt))
secax.set_ylabel("vel. error [m/s]", labelpad=15)

# Embellish figure.

ax1.set_xlim([0.9 * nsrc[0], 1.1 * nsrc[-1]])
ax1.set_ylim([0.0, 0.25])
plt.tight_layout()
plt.grid()

plt.show()
