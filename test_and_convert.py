import pathlib
import subprocess


ALL_NOTEBOOKS = [
    "SRT_inversion.py",
    "SparsityEnforcingRegularisation.py",
    "analytic_2D.py",
    "analytic_2D_looped.py",
]


for nb in ALL_NOTEBOOKS:
    print(f"Processing notebook {nb}")
    input_filename = pathlib.Path(nb)
    output_filename = pathlib.Path(
        input_filename.parent,
        input_filename.with_suffix(".ipynb").name.replace(" ", "_").replace("'", ""),
    )
    p = subprocess.run(
        [
            "jupytext",
            "--to",
            "ipynb",
            "-o",
            str(output_filename),
            str(input_filename),
        ],
        capture_output=True,
    )

    if p.returncode != 0:
        print("stdout\n++++++++++++++++++++++", p.stdout)
        print("--------------------------------------------------------------")
        print("stderr\n++++++++++++++++++++++", p.stderr)
        raise ValueError(f"Converting {input_filename} to ipynb failed.")

    p = subprocess.run(
        [
            "jupyter",
            "nbconvert",
            "--execute",
            "--inplace",
            "--ExecutePreprocessor.timeout=-1",
            "--ExecutePreprocessor.store_widget_state=False",
            str(output_filename),
        ],
        capture_output=True,
    )

    if p.returncode != 0:
        print("stdout\n++++++++++++++++++++++", p.stdout)
        print("--------------------------------------------------------------")
        print("stderr\n++++++++++++++++++++++", p.stderr)
        raise ValueError(f"Executing {output_filename} failed.")

    p = subprocess.run(
        [
            "jupyter",
            "nbconvert",
            "--to",
            "markdown",
            "--output-dir=website/docs/notebooks",
            str(output_filename),
        ],
        capture_output=True,
    )

    if p.returncode != 0:
        print("stdout\n++++++++++++++++++++++", p.stdout)
        print("--------------------------------------------------------------")
        print("stderr\n++++++++++++++++++++++", p.stderr)
        raise ValueError(f"Converting {output_filename} to markdown failed.")
