
# Diffuse ultrasound computed tomography for medical imaging
This repository contains a collection of notebooks that accompany the paper **Diffuse ultrasound computed tomography for medical imaging**. Figures labelled with [R] can be reproduced in the notebooks. You can run the examples directly in your browser [here](https://mybinder.org/v2/gl/swp_ethz%2Fpublic%2Frandom-field-interferometry/master) 

Note that in order to reproduce the figures exactly, CI settings need to be disabled in the notebook (relevant cells are labelled). 

For further questions, feel free to contact Ines Elisa Ulrich (ines.ulrich@erdw.ethz.ch).


